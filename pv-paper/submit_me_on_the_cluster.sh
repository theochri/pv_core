#!/bin/bash
# SBATCH --account=rpp-revins
# SBATCH --time=00:30:00
# SBATCH --cpus-per-task=1
# SBATCH --mem=2000mb
# SBATCH --output=output_slurm/%x-%j.out


# Rerunning to fill in

# 200 runs - Cost fill in
sbatch --array=2-8 --time=20:00:00 cluster_batch.sh

# 400 runs - net-import fill in.
sbatch --array=37-39 --time=40:00:00 cluster_batch.sh

# 800 runs
sbatch --array=45-59 --time=80:00:00 cluster_batch.sh
