#!/bin/bash
#SBATCH --account=rpp-revins
#SBATCH --cpus-per-task=1
#SBATCH --mem=4000mb
#SBATCH --output=output_slurm/%x-%j.out

module load StdEnv/2020
module load energyplus/9.3.0
module load gurobi
module load python/3.6  
module load geos/3.8.1 
module load gcc/10.2.0

source ~/paper_pusher/bin/activate

echo "prog started at: `date`"
echo "Starting task $SLURM_ARRAY_TASK_ID"

sleep $SLURM_ARRAY_TASK_ID

python cluster_job_assigner.py $SLURM_ARRAY_TASK_ID

deactivate
echo "prog ended at: `date`"
