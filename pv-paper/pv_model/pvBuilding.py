from besos.problem import EPProblem, Problem
from besos.evaluator import EvaluatorEP
from besos.objectives import MeterReader
from besos import eppy_funcs as ef
from datetime import datetime, timedelta
from time import time
from calendar import monthrange

import calendar
import os
import sqlite3
import pvlib

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import logging

logger = logging.getLogger()
logger.setLevel(logging.ERROR)


class PvBuilding:
    """
    This class that sets up the EnergyPlus simulation for the PV-paper.
    """

    def __init__(self, idf_file, out_dir, epw_file, err_dir, **kwargs):
        """
        :param idf_file: The idf file of the EP simulation
        :param out_dir:  Provides EP with an output directory
        :param epw_file: The epw file of the EP simulation
        :param err_dir:  Provides EP with an error directory
        """

        self.idf_file = idf_file
        self.output_dir = out_dir
        self.epw_file = epw_file
        self.error_dir = err_dir
        self.sql_file = out_dir + "eplusout.sql"
        # Load the idf file
        try:
            print(kwargs["ep_path"])
            self.building = ef.get_building(
                building=self.idf_file, ep_path=kwargs["ep_path"]
            )
        except:
            self.building = ef.get_building(building=self.idf_file)
        # Use this to print just the design day's specified in the idf
        # Grab the North axis of the building
        self.Building_Information = self.building.idfobjects["BUILDING"][0]
        self.North_Axis = self.Building_Information.North_Axis
        # print(building.idfobjects['SizingPeriod:DesignDay'])
        # Assign these to a new object to change the settings
        # self.sizing_period_day_array = self.building.idfobjects['SizingPeriod:DesignDay']
        # You can print for example the month of the first design day
        # print(sizing_period_day_array[1].Month)
        # We wish to have only the epw modelled therefore,
        # Grab the Simulation Control settings
        self.building_setting = self.building.idfobjects["SIMULATIONCONTROL"][0]
        # Overwrite the Yes setting to No
        self.building_setting.Run_Simulation_for_Sizing_Periods = "No"

    def max_demand(self):
        """
        This function calculates the maximum yearly
        charge rate based on the large general service.
        """
        # This creates an array with the abbrivations of the month ["Jan", "Feb"..]
        self.billing_periods = [
            "{}".format(calendar.month_abbr[month_val]) for month_val in range(1, 13)
        ]
        # This section creates bins of hours to bin the hourly data into months.
        # starting 0
        self.hours_list = [0]
        for i in range(11):
            days = monthrange(2011, i + 1)[1]
            self.hours_list.append(days * 24 + self.hours_list[i])
        # Ending one before 8761
        self.hours_list.append(8761)
        # Cut up the hourly data
        # First bin the hourly data
        self.bins = pd.cut(
            self.hourly_electricity.index,
            bins=self.hours_list,
            right=False,
            labels=self.billing_periods.copy(),
        )
        # Make a new dataframe
        self.elec_months = pd.DataFrame(
            self.hourly_electricity.values, columns=["Electricty"]
        )
        # add the months column assigning every hour a month label.
        self.elec_months["months"] = self.bins
        # find the maxium value for each month
        self.monthly_peak_demand = self.elec_months.groupby("months").max()
        # calculate hte maximum demand charge based on the sum times the $ 12.34 / kW
        max_sum_charge = self.monthly_peak_demand.sum()[0] * 12.34
        # print out
        print("The maximum demand charge is: $ {:,.2f}".format(max_sum_charge))
        # return to be used in the PvModel
        self.pre_demand_maximum = self.monthly_peak_demand.max()
        # Winter maximum demand
        df_months = self.monthly_peak_demand
        # select just the winter months
        df_months1 = df_months.loc[
            lambda df_months: ["Jan", "Feb", "Mar", "Nov", "Dec"], :
        ]
        # find the maximum
        self.pre_winter_maximum = df_months1.max()
        return max_sum_charge

    def run_ep(self, **kwargs):
        """
        Minimal EnergyPlus Run Function and store the electricity use for the entire year.
        """
        # Build the objectives
        ElectricityMeter = MeterReader("Electricity:Facility", func=self.timeseriesfunc)
        EPobjectives = [ElectricityMeter]
        # Build the besos evaluator
        try:
            print("EnergyPlus is found in the path: {}".format(kwargs["ep_path"]))
            evaluator = EvaluatorEP(
                EPProblem(outputs=EPobjectives),
                self.building,
                out_dir=self.output_dir,
                epw_file=self.epw_file,
                err_dir=self.error_dir,
                ep_path=kwargs["ep_path"],
                version="9.3",
            )
        except:
            evaluator = EvaluatorEP(
                EPProblem(outputs=EPobjectives),
                self.building,
                out_dir=self.output_dir,
                epw_file=self.epw_file,
                err_dir=self.error_dir,
                version="9.3",
            )
        # Run evaluator
        time_series = evaluator([])
        # wrap everything in a nice dataframe for export
        time_series_df = pd.DataFrame.from_dict(
            dict(time_series[0]), orient="index", columns=["Electricity:Facility"]
        )
        # Convert to kWh.
        for i in range(len(time_series_df["Electricity:Facility"])):
            time_series_df["Electricity:Facility"][i] = time_series_df[
                "Electricity:Facility"
            ][i] / (3.6 * 10 ** (6))

        self.hourly_electricity = time_series_df["Electricity:Facility"]

    def timeseriesfunc(self, result):
        """ This is a function that grabs the timeseries for the Meter Reader  """
        return result.data["Value"]

    def parse(self, fn, frequency="hourly"):
        """
        Read EnergyPlus SQLite database into a DataFrame.
        Parameters
        ----------
        fn : path-like object
            EnergyPlus SQL output to read
        frequency : str, default 'hourly'
            Fetch data with this reporting frequency
        Returns
        -------
        DataFrame

        Use:
        building.parse(sql_file)
        building.parsed_df.head()
        """

        if not os.path.exists(fn):
            raise OSError("File not found: %s" % fn)

        frequency = frequency.lower().capitalize()
        connection = sqlite3.connect(fn)

        # Construct SQL query
        data_sql = """
            SELECT KeyValue, VariableName, VariableValue, SimulationDays, \
                    Time.TimeIndex
            FROM ReportVariableData
            INNER JOIN ReportVariableDataDictionary
            ON ReportVariableDataDictionary.ReportVariableDataDictionaryIndex \
                    = ReportVariableData.ReportVariableDataDictionaryIndex
            INNER JOIN Time
            ON Time.TimeIndex = ReportVariableData.TimeIndex
            WHERE ReportingFrequency == ?
        """
        raw_df = pd.read_sql(data_sql, connection, params=(frequency,))

        connection.close()
        # building.parsed_df['','Electricity:Facility'].values
        # Should get you the same as the timeseries data from above.
        self.parsed_df = raw_df.pivot_table(
            values="VariableValue",
            index=["TimeIndex"],
            columns=["KeyValue", "VariableName"],
        )

    def get_roof_details(self):
        """ This obtains all the gross areas of the roof and wall of the structure """
        # Connect to SQL
        conn = sqlite3.connect(self.sql_file)
        print(self.sql_file)
        # Define Queries
        def select_all_from_table(table_name, conn):
            q = "SELECT * FROM {}".format(table_name)
            return pd.read_sql(q, conn)

        def select_tabular_report(report_name, conn, table_name=None):
            q = "SELECT * FROM TabularDataWithStrings WHERE ReportName='{}'".format(
                report_name
            )
            if table_name:
                q = q + " and TableName='{}'".format(table_name)
            return pd.read_sql(q, conn)

        better = select_all_from_table("Surfaces", conn)
        Roof = better[(better["ClassName"] == "Roof") & (better["ExtSolar"] == 1)].sum()
        Roof_height = better[
            (better["ClassName"] == "Roof") & (better["ExtSolar"] == 1)
        ].max()
        walls = (
            better[(better["ClassName"] == "Wall") & (better["ExtSolar"] == 1)]
            .groupby("Azimuth")
            .sum()
        )

        # Roof
        self.roof_area = Roof["Area"]
        self.roof_height = Roof_height["Height"]
        # North
        self.northwall_area = walls["Area"][0.0]
        # East
        self.eastwall_area = walls["Area"][90.0]
        # South
        self.southwall_area = walls["Area"][180.0]
        # West
        self.westwall_area = walls["Area"][270.0]

    def get_floor_area(self):
        conn = sqlite3.connect(self.sql_file)
        # Define Queries
        def select_all_from_table(table_name):
            q = "SELECT * FROM {}".format(table_name)
            return pd.read_sql(q, conn)

        def select_tabular_report(report_name, table_name=None):
            q = "SELECT * FROM TabularDataWithStrings WHERE ReportName='{}'".format(
                report_name
            )
            if table_name:
                q = q + " and TableName='{}'".format(table_name)
            return pd.read_sql(q, conn)

        better = select_all_from_table("Zones")
        self.total_floorarea = np.sum(better["FloorArea"])
        print("Total surface area {:,.0f} m2".format(self.total_floorarea))

    def p_roof_details(self):
        print("Roof area is {:,.0f} m2".format(self.roof_area))
        print("Roof area is up {:,.0f} m high".format(self.roof_height))

    def get_weather(self, year=None):

        if year:
            self.weather, self.meta = pvlib.iotools.read_epw(
                self.epw_file, coerce_year=year
            )
        else:
            self.weather, self.meta = pvlib.iotools.read_epw(self.epw_file)

    def check_power(self):
        self.electricity_max = np.max(self.hourly_electricity)
        self.electricity_min = np.min(self.hourly_electricity)
        self.electricity_mean = np.mean(self.hourly_electricity)
        self.electricity_sum = np.sum(self.hourly_electricity)
        print("The maximum power used is {:,.0f} kW".format(self.electricity_max))
        print("The minimum power used is {:,.0f} kW".format(self.electricity_min))
        print("The average power used is {:,.0f} kW".format(self.electricity_mean))
        print(
            "The total Electricity use is {:,.0f} kWh or {:,.0f} MWh".format(
                self.electricity_sum, self.electricity_sum / 1000
            )
        )

    def check_bc_rate(self):
        self.check_power()
        # Electricity use is above 115kW or sum total electricity is above 550,000kWh
        if (self.electricity_max > 150) | (self.electricity_sum > 550000):
            print("This building is in the **Large General Service** Rate")
            rate = "large"
        elif (
            (self.electricity_max > 35)
            & (self.electricity_max <= 150)
            & (self.electricity_sum <= 550000)
        ):
            print("This building is in the **Medium General Service** Rate")
            rate = "medium"
        elif self.electricity_max <= 35:
            print("This building is **Small General Service** Rate")
            rate = "small"
        return rate

    def summerize_the_building(self):
        print(self.idf_file)
        self.hourly_electricity.plot()
        plt.show()
        self.get_roof_details()
        self.p_roof_details()
        self.get_floor_area()
        self.check_bc_rate()
        Electricity_per_surface = self.electricity_sum / self.total_floorarea
        print(
            "Electricity per surface area: {:.0f} kWh/m2".format(
                Electricity_per_surface
            )
        )
        print(
            "Electricity per surface area: {:.4f} GJ/m2".format(
                Electricity_per_surface / 277.778
            )
        )
