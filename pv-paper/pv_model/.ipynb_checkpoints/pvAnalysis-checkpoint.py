from pathlib import PurePath
import glob
import pandas as pd
from pandas.core.common import flatten
import re
# library
import numpy as np
import matplotlib.pyplot as plt
import ast
import matplotlib as mpl
import seaborn as sns

import warnings
warnings.filterwarnings("ignore")
font = {'family' : 'DejaVu Sans',
        'weight' : 'regular',
        'size'   : 14}
axis_font = {'fontname':'DejaVu Sans', 'size':'14', 'weight':'bold'}
mpl.rc('font', **font)


class PvAnalyzer:

    def __init__(self,path):
        # This is path where all the GA results are stored.
        self.PATH = path



    def grab_processed_files_to_df(self):
        """
        This function pulls all the different processed files from the
        nested directories. It puts all the available processed files
        in a dataframe that constains:
         --- The filename
         --- Location - as specified by the directory
         --- Building Name - as specified by the directory
         --- Run - as specified by directory
         --- Price - taken from the filename
         --- Obj - taken from the filename
        """
        # this pulls all the file names with all the directories
        files = [f for f in glob.glob(self.PATH + "**/*.xlsx", recursive=True)]
        #print(files)

        # Start the dataframe
        column_names = ["Filename","Location","Building Name","Run","Price","Obj"]
        self.processed_df = pd.DataFrame(columns=column_names)

        # Step through all the xlsx files found
        for f in files:
            # We use pure path to grab all the bits really quickly.
            p = PurePath(f)
            parts_of_filename = p.parts
            # This makes sure it only looks at the results that are not in the custom folders like 20pop_test_run, but looks at the bc->building->run
            if len(parts_of_filename)>=5:
                # Split the filename _ & . into parts
                filenametest=re.split('_|\.',parts_of_filename[-1])
                # Processed is added as the as the second last part.
                # Check if this is added to the file name.
                if any(x in "processed" for x in filenametest):
                    #print(parts_of_filename)
                    #print(filenametest)
                    obj = filenametest[2]
                    # Build the list
                    list_to_df = [[f,parts_of_filename[-4],parts_of_filename[-3],parts_of_filename[-2],filenametest[1],obj]]
                    # Build the list into a row that we will append.
                    row_to_append = pd.DataFrame(list_to_df,columns=column_names)
                    # Append
                    self.processed_df = self.processed_df.append(row_to_append,ignore_index=True)

    def fuse_and_create_dual_from_rbf(self):
        """
        This function goes through and finds all rbf outputs per run, per building
        """

    def turn_Obj(self):
        """
        This function makes sure the Obj column is only sing/dual labels.
        """
        self.processed_df["original_Obj"]=self.processed_df["Obj"]
        self.processed_df=self.processed_df.replace({'Obj': {"rbf-cost": "dual", "rbf-net": "dual","ga-20": "dual","ga-50": "dual","ga-100": "dual",}})


    def append_cheapest_solution(self):
        """
        This function just grabs the cheapest solution of each processed
        solution. Builds a new DF to be used for plots that constains all
        the results. cheapest_solution_df
        """
        # Make a deep copy of the processed_df
        self.cheapest_solution_df =  self.processed_df.copy()
        # Start iterating over the processed files to grab the cheapest solution
        for i in range(len(self.processed_df)):
            # Pull out the file name
            file_name=self.processed_df['Filename'][i]
            # Read the excel file
            df = pd.read_excel(io=file_name)
            # Grab the row with the cheapest solution
            row_lowest_cost=df.loc[df['total_cost'].idxmin()]
            #print(row_lowest_cost)
            # Remove the numerical integer
            try:
                row_lowest_cost.pop("Unnamed: 0")
            except KeyError:
                print("keyerror")
            finally:
                # removes the "Unnamed: 0"
                # Take all the values and append them to the dataframe.
                for j in row_lowest_cost.keys():
                    self.cheapest_solution_df.loc[i,j]=row_lowest_cost[j]

    def append_lowestimport_solution(self):
        """
        This function just grabs the cheapest solution of each processed
        solution. Builds a new DF to be used for plots that constains all
        the results. cheapest_solution_df
        """
        # Make a deep copy of the processed_df
        self.lowest_import =  self.processed_df.copy()
        # Start iterating over the processed files to grab the cheapest solution
        for i in range(len(self.processed_df)):
            # Pull out the file name
            file_name=self.processed_df['Filename'][i]
            # Read the excel file
            df = pd.read_excel(io=file_name)
            # Grab the row with the cheapest solution
            row_lowest_cost=df.loc[df['Energy Imported'].idxmin()]
            # Remove the numerical integer
            try:
                row_lowest_cost.pop("Unnamed: 0")
            except KeyError:
                print("keyerror")
            finally:
                # removes the "Unnamed: 0"
                # Take all the values and append them to the dataframe.
                for j in row_lowest_cost.keys():
                    self.lowest_import.loc[i,j]=row_lowest_cost[j]



    def build_price_graph(self,building_name,location,Obj,use_x_as_labels=False, legend_loc='lower right',demand=False, roof_top_used=False,roof_top_constraint=False,Run="43"):
        """
        This function builds a simple plot of how the total cost and
        energy generated changes with
        """

        ###############

        broof_area = {"SmallOffice":599,
                    "Warehouse":4598,
                    "MediumOffice":1661,
                    "LargeOffice":3563}

        belectricity = {"SmallOffice":61846,
                    "Warehouse":261136,
                    "MediumOffice":695621,
                    "LargeOffice":5141397}


        self.sub_set = self.cheapest_solution_df.loc[\
                                    (self.cheapest_solution_df["Building Name"]==building_name) &\
                                    (self.cheapest_solution_df["Location"]==location) & \
                                    (self.cheapest_solution_df["Run"]==Run) &\
                                    (self.cheapest_solution_df["Obj"]==Obj) ].sort_values("Price", ascending=False)

        k=Run
        # Select the data we wish to plot
        self.selected_df = self.cheapest_solution_df.loc[\
                                (self.cheapest_solution_df["Building Name"]==building_name) &\
                                (self.cheapest_solution_df["Location"]==location) & \
                                (self.cheapest_solution_df["Run"]==k) & \
                                (self.cheapest_solution_df["Obj"]==Obj) ].sort_values("Price", ascending=False)
        # Build up the values from the cost break.
        y=[]
        # get the cost break
        cost_break = self.selected_df["Cost Break"]
        for i in range(len(cost_break)):
            # Convert them to dicts
            each_cost_break = ast.literal_eval(cost_break.iloc[i])
            # Put them in the y
            y.append(list(each_cost_break.values()))

        # The labels for the stackplot will come from the keys.
        labels = list(each_cost_break.keys())


        # Stack them to plot
        y = np.vstack(y)
        # Rotate to make it work nicely with stackplot
        y = np.transpose(y)
        # Grab the energy produce per panel
        y2 = self.selected_df["Panel Energy Produced"]
        # Build the x-axis
        if use_x_as_labels:
            # From the import on the excel we get strings
            x=self.selected_df["Price"]
        else:
            # by default I converted them to floats.
            x =  [float(self.selected_df["Price"].iloc[i]) for i in range(len(self.selected_df))]
        axis_title_size=20

        # Start the figure
        fig = plt.figure(figsize=(8, 6))
        ax1 = plt.subplot(1, 1, 1)
        plt.tick_params(labelsize=14, direction="in")
        # Make sure to plot from high to low
        plt.gca().invert_xaxis()
        ax1.set_xlabel('Price of the panel ($/m²)', fontsize=axis_title_size)
        ax1.set_ylabel('Total Cost (10³ $ CAD / year)', fontsize=axis_title_size)  # we already handled the x-label with ax1
        max_val_y = y.sum(axis=0).max() # First sum all the energies, than take the max
        lns1 = ax1.stackplot(x, y/1000, labels=labels,alpha=0.7)
        ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
        color = 'C0'
        max_val_y2 = max(y2/1000)
        ax2.set_ylim([0,max_val_y2+max_val_y2*0.05])
        lns2 = ax2.plot(x, y2/1000, color=color, marker='o', label="PV Panel")
        ax2.set_ylabel('Energy Generated (MWh)', color=color, fontsize=axis_title_size)  # we already handled the x-label with ax1

        ax2.tick_params(axis='y', colors=color,direction="in",labelsize=14)

        if demand:
            ax2.axhline(belectricity[building_name]/1000,color=color,linestyle="dotted")
            max_val_y2 = max(list(flatten([y2/1000,belectricity[building_name]/1000])))
            ax2.set_ylim([0,max_val_y2+max_val_y2*0.03])



        #ax1.yaxis.set_major_formatter(plt.FuncFormatter(lambda x, loc: "{:,}".format(int(x/1000))))
        #ax2.yaxis.set_major_formatter(plt.FuncFormatter(lambda x, loc: "{:,}".format(int(x/1000))))
        ax2.grid(False)
        lns3= []
        color = 'grey'
        if any([roof_top_used,roof_top_constraint]):
            y3=self.selected_df['Roof Area Used']
            ax3 = ax1.twinx()
            ax3.grid(False)
            lns3 = ax3.plot(x, y3, color=color, marker='x', label="PV Used Area")
            ax3.spines["right"].set_position(("axes", 1.2))
            ax3.set_ylabel('Roof Top Area Used (m²)', color=color, fontsize=axis_title_size)
            ax3.tick_params(axis='y', colors=color,direction="in",labelsize=14)
            max_val_y3 = max(y3)
            ax3.set_ylim([0,max_val_y3+max_val_y3*0.05])
            if roof_top_constraint:
                ax3.axhline(broof_area[building_name], linestyle="dashed", color=color)
                max_val_y3 = max(list(flatten([y3,broof_area[building_name]])))
                ax3.set_ylim([0,max_val_y3+max_val_y3*0.10])

        lns = lns1+lns2+lns3
        labs = [l.get_label() for l in lns]


        legend=ax1.legend(lns, labs, loc=legend_loc, facecolor='white',framealpha=1)
        ax1.set_ylim([0,max_val_y+max_val_y*0.05])

        plt.title('Building Name: {}'.format(building_name), fontsize=axis_title_size)
        plt.show()

        return ax1,ax2,ax3

    def process_axs(self,ax,building_name,location,Obj,annot,k="45",use_x_as_labels=False,
            legend_loc='lower right', demand=True, roof_top_used=True,roof_top_constraint=True,font=font):
        """
        This function processes an axis of the four results plot to put those nice graphics
        """

        ###############

        broof_area = {"SmallOffice":599,
                    "Warehouse":4598,
                    "MediumOffice":1661,
                    "LargeOffice":3563}

        belectricity = {"SmallOffice":61846,
                    "Warehouse":261136,
                    "MediumOffice":695621,
                    "LargeOffice":5141397}

        blabel = {"SmallOffice":"Small Office",
                    "Warehouse":"Warehouse",
                    "MediumOffice":"Medium Office",
                    "LargeOffice":"Large Office"}

        self.sub_set = self.cheapest_solution_df.loc[\
                                    (self.cheapest_solution_df["Building Name"]==building_name) &\
                                    (self.cheapest_solution_df["Location"]==location) & \
                                    (self.cheapest_solution_df["Obj"]==Obj) ].sort_values("Price", ascending=False)

        for k in [k]:
            # Select the data we wish to plot
            self.selected_df = self.cheapest_solution_df.loc[\
                                    (self.cheapest_solution_df["Building Name"]==building_name) &\
                                    (self.cheapest_solution_df["Location"]==location) & \
                                    (self.cheapest_solution_df["Run"]==k) & \
                                    (self.cheapest_solution_df["Obj"]==Obj) ].sort_values("Price", ascending=False)
            # Build up the values from the cost break.
            y=[]
            # get the cost break
            cost_break = self.selected_df["Cost Break"]
            for i in range(len(cost_break)):
                # Convert them to dicts
                each_cost_break = ast.literal_eval(cost_break.iloc[i])
                # Put them in the y
                y.append(list(each_cost_break.values()))

            # The labels for the stackplot will come from the keys.
            labels = list(each_cost_break.keys())

            # Stack them to plot
            y = np.vstack(y)
            # Rotate to make it work nicely with stackplot
            y = np.transpose(y)
            # Grab the energy produce per panel
            y2 = self.selected_df["Panel Energy Produced"]
            # Build the x-axis
            if use_x_as_labels:
                # From the import on the excel we get strings
                x=self.selected_df["Price"]
            else:
                # by default I converted them to floats.
                x =  [float(self.selected_df["Price"].iloc[i]) for i in range(len(self.selected_df))]
            axis_title_size=20
            ytemp=np.copy(y)
            max_val_y = ytemp.sum(axis=0).max() # First sum all the energies, than take the max
            ax.set_ylim([0,max_val_y/1000+max_val_y/1000*0.05])
            lns1 = ax.stackplot(x, y/1000, labels=labels,alpha=0.7)
            ax2 = ax.twinx()  # instantiate a second axes that shares the same x-axis
            color = 'C0'
            max_val_y2 = max(list(flatten([y2/1000,belectricity[building_name]/1000])))
            if max_val_y2>1000:
                max_val_y2=max_val_y2/1000
                ax2.set_ylim([0,max_val_y2+max_val_y2*0.05])
                lns2 = ax2.plot(x, y2/1000000, color=color, marker='o', label="PV Panel")
                ax2.set_ylabel('Energy Generated (GWh)', color=color, fontdict=font)  # we already handled the x-label with ax1
            else:
                ax2.set_ylim([0,max_val_y2+max_val_y2*0.05])
                lns2 = ax2.plot(x, y2/1000, color=color, marker='o', label="PV Panel")
                ax2.set_ylabel('Energy Generated (MWh)', color=color, fontdict=font)  # we already handled the x-label with ax1

            ax2.tick_params(axis='y', colors=color,direction="in")

            if demand:
                max_val_y2 = max(list(flatten([y2/1000,belectricity[building_name]/1000])))
                if max_val_y2>1000:
                    lns5=ax2.axhline(belectricity[building_name]/1000000,color=color, linewidth=2, linestyle="dotted", label="Total Electricity")
                    max_val_y2=max_val_y2/1000
                else:
                    lns5=ax2.axhline(belectricity[building_name]/1000,color=color, linewidth=2, linestyle="dotted", label="Total Electricity")
                ax2.set_ylim([0,max_val_y2+max_val_y2*0.03])

            ax2.grid(False)
            lns3= []
            color = 'grey'
            set_as_percentage=True
            if set_as_percentage:
                if any([roof_top_used,roof_top_constraint]):
                    y3=self.selected_df['Roof Area Used']
                    ax3 = ax.twinx()
                    # 100% line
                    lns4=ax3.axhline(100, linestyle="dotted", linewidth=2, color=color, label="100% Roof Use")
                    max_val_y3 = max(list(flatten([y3,broof_area[building_name]])))
                    ax3.grid(False)
                    lns3 = ax3.plot(x, y3/max_val_y3*100, color=color, marker='^', label="PV Used Area")
                    ax3.spines["right"].set_position(("axes", 1.25))
                    ax3.set_ylabel('Roof Top Area Used (%)', color=color, fontdict=font)
                    ax3.tick_params(axis='y', colors=color,direction="in")
                    ax3.set_ylim([0,100+100*0.10])                
            else:
                # Use absolute values
                if any([roof_top_used,roof_top_constraint]):
                    y3=self.selected_df['Roof Area Used']
                    ax3 = ax.twinx()
                    ax3.grid(False)
                    lns3 = ax3.plot(x, y3, color=color, marker='x', label="PV Used Area")
                    ax3.spines["right"].set_position(("axes", 1.25))
                    ax3.set_ylabel('Roof Top Area Used (m²)', color=color)
                    ax3.tick_params(axis='y', colors=color,direction="in")
                    max_val_y3 = max(y3)
                    ax3.set_ylim([0,max_val_y3+max_val_y3*0.05])
                    if roof_top_constraint:
                        # This is the total area of the roof
                        lns4=ax3.axhline(broof_area[building_name], linestyle="dotted", linewidth=2, color=color, label="Total Roof Area")
                        max_val_y3 = max(list(flatten([y3,broof_area[building_name]])))
                        ax3.set_ylim([0,max_val_y3+max_val_y3*0.10])

            lns = lns1+lns2+lns3

            ax.set_title('{} {}'.format(annot,blabel[building_name]),fontdict=font)
        ax.invert_xaxis()
        ax.set_xlim(400,100)

        return ax,[ax,ax2,ax3]


    def process_axs_low(self,ax,building_name,location,Obj,annot,k="45",use_x_as_labels=False,
    legend_loc='lower right', demand=True, roof_top_used=True,roof_top_constraint=True):
        """
        This function processes an axis of the four results plot to put those nice graphics
        """

        ###############

        broof_area = {"SmallOffice":599,
                    "Warehouse":4598,
                    "MediumOffice":1661,
                    "LargeOffice":3563}

        belectricity = {"SmallOffice":61846,
                    "Warehouse":261136,
                    "MediumOffice":695621,
                    "LargeOffice":5141397}

        blabel = {"SmallOffice":"Small Office",
                    "Warehouse":"Warehouse",
                    "MediumOffice":"Medium Office",
                    "LargeOffice":"Large Office"}

        self.sub_set = self.lowest_import.loc[\
                                    (self.lowest_import["Building Name"]==building_name) &\
                                    (self.lowest_import["Location"]==location) & \
                                    (self.lowest_import["Obj"]==Obj) ].sort_values("Price", ascending=False)

        for k in [k]:
            # Select the data we wish to plot
            self.selected_df = self.lowest_import.loc[\
                                    (self.lowest_import["Building Name"]==building_name) &\
                                    (self.lowest_import["Location"]==location) & \
                                    (self.lowest_import["Run"]==k) & \
                                    (self.lowest_import["Obj"]==Obj) ].sort_values("Price", ascending=False)
            # Build up the values from the cost break.
            y=[]
            # get the cost break
            cost_break = self.selected_df["Cost Break"]

            for i in range(len(cost_break)):
                # Convert them to dicts
                each_cost_break = ast.literal_eval(cost_break.iloc[i])
                # Put them in the y
                y.append(list(each_cost_break.values()))

            # The labels for the stackplot will come from the keys.
            labels = list(each_cost_break.keys())

            # Stack them to plot
            y = np.vstack(y)
            # Rotate to make it work nicely with stackplot
            y = np.transpose(y)
            # Grab the energy produce per panel
            y2 = self.selected_df["Panel Energy Produced"]
            # Build the x-axis
            if use_x_as_labels:
                # From the import on the excel we get strings
                x=self.selected_df["Price"]
            else:
                # by default I converted them to floats.
                x =  [float(self.selected_df["Price"].iloc[i]) for i in range(len(self.selected_df))]
            axis_title_size=20
            ytemp=np.copy(y)
            max_val_y = ytemp.sum(axis=0).max() # First sum all the energies, than take the max
            ax.set_ylim([0,max_val_y/1000+max_val_y/1000*0.05])
            lns1 = ax.stackplot(x, y/1000, labels=labels,alpha=0.7)
            ax2 = ax.twinx()  # instantiate a second axes that shares the same x-axis
            color = 'C0'
            max_val_y2 = max(y2/1000)
            ax2.set_ylim([0,max_val_y2+max_val_y2*0.05])
            lns2 = ax2.plot(x, y2/1000, color=color, marker='o', label="PV Panel")
            ax2.set_ylabel('Energy Generated (MWh)', color=color)  # we already handled the x-label with ax1

            ax2.tick_params(axis='y', colors=color,direction="in")

            if demand:
                lns5=ax2.axhline(belectricity[building_name]/1000,color=color, linewidth=2, linestyle="dotted", label="Total Electricity")
                max_val_y2 = max(list(flatten([y2/1000,belectricity[building_name]/1000])))
                ax2.set_ylim([0,max_val_y2+max_val_y2*0.03])

            ax2.grid(False)
            lns3= []
            color = 'grey'
            if any([roof_top_used,roof_top_constraint]):
                y3=self.selected_df['Roof Area Used']
                ax3 = ax.twinx()
                ax3.grid(False)
                lns3 = ax3.plot(x, y3, color=color, marker='x', label="PV Used Area")
                ax3.spines["right"].set_position(("axes", 1.25))
                ax3.set_ylabel('Roof Top Area Used (m²)', color=color)
                ax3.tick_params(axis='y', colors=color,direction="in")
                max_val_y3 = max(y3)
                ax3.set_ylim([0,max_val_y3+max_val_y3*0.05])
                if roof_top_constraint:
                    lns4=ax3.axhline(broof_area[building_name], linestyle="dotted", linewidth=2, color=color, label="Total Roof Area")
                    max_val_y3 = max(list(flatten([y3,broof_area[building_name]])))
                    ax3.set_ylim([0,max_val_y3+max_val_y3*0.10])

            lns = lns1+lns2+lns3

            plt.title('{} {}'.format(annot,blabel[building_name]))
        ax.invert_xaxis()
        ax.set_xlim(400,100)

        return ax,[ax,ax2,ax3]

    def what_is_found_by_ga_low(self,building_name,location,Obj,price,sep_axis=False,hista=True,Run="45"):
        """
        This function goes and finds what is found by the GA on the pareto front. It then looks at
        what is installed and tallies up the angle, tilt and gcr of all the solutions to make a
        density plot. We also show were the cheapest option is found.

        args:
            building_name
            location
            Obj
            price
            sep_axis
            hista
        returns:
            A nice plot, what is found in the last population in by the GA.
        """


        ###############
        # Select the data we wish to plot
        self.selected_df = self.processed_df.loc[\
                                (self.processed_df["Building Name"]==building_name) &\
                                (self.processed_df["Location"]==location) & \
                                (self.processed_df["Run"]==Run) & \
                                (self.processed_df["Obj"]==Obj)]
        # To build the density plot we will construct a pv_panel_list
        # This will contain all panels found for the supplied metrics.
        # We also include total cost of the system to highlight the
        # location of the cheapest solution in the density plot.
        col_names = ["Capacity","Azimuth","Tilt","GCR","Total_cost","Energy Imported","price"]
        pv_panel_df = pd.DataFrame(columns=col_names)
        # Find all the runs
        for i in range(len(self.selected_df)):
            df = pd.read_excel(io=self.selected_df["Filename"].iloc[i])
            # Each run we will open the processed excel file.
            for j in range(len(df)):
                # Each row we will find the panels that are installed and
                # add them to the pv_panel_list
                # Find the number of panels

                colnames_=list(df.columns)
                azi_names=[name_ for name_ in colnames_ if name_[:3]=="azi"]

                for k in range(len(azi_names)):
                    row_panel = []
                    row_panel.append(df["cap{}".format(k)].iloc[j])
                    row_panel.append(df["azi{}".format(k)].iloc[j])
                    row_panel.append(df["tilt{}".format(k)].iloc[j])
                    row_panel.append(df["gcr{}".format(k)].iloc[j])
                    row_panel.append(df["total_cost"].iloc[j])
                    row_panel.append(df["Energy Imported"].iloc[j])
                    row_panel.append(self.selected_df["Price"].iloc[i])
                    row_panel_df=pd.DataFrame([row_panel],columns=col_names)
                    pv_panel_df=pv_panel_df.append(row_panel_df,ignore_index=True)

        # Simple data to display in various forms
        opt_sol=pd.DataFrame([],columns=pv_panel_df.columns)
        for i, val in enumerate(price):
            panels_at_price = pv_panel_df.loc[pv_panel_df["price"]==val]

            # try to find the minimal solution
            # There might not be a minimal solution
            id_min = panels_at_price["Energy Imported"].loc[
                panels_at_price["Capacity"]>=0] == panels_at_price["Energy Imported"].min()
            cheapest_panel_row=panels_at_price.loc[id_min]
            opt_sol=opt_sol.append(cheapest_panel_row,ignore_index=True)
        print(opt_sol)
        # This will make sure the bars are stacked on top of each other by determing if
        # there is a bottom value of the bar. This bottom value is based on the if a
        # same setting is found at a higher price. If there is lower price with the same
        # setting we will stack this on top of the other bar by setting the bottom
        # variable.
        for i, parameter in enumerate(['Azimuth','Tilt','GCR']):
            # By default the bottom will be set to zero
            opt_sol["Bottom {}".format(parameter)] = 0
            # Then check the solutions in order of occurance
            for j,sol in enumerate(opt_sol[parameter]):
                # are there any solutions before using the same setting for the parameter
                arethereanybefore = any(opt_sol[parameter].iloc[0:j]==sol)
                # there are:
                if arethereanybefore:
                    # Check all the previous solutions to find those with the same setting
                    check_prev    = opt_sol[parameter].iloc[0:j]==sol
                    # Loop through all those, and build a list of them.
                    list_of_prevs = [opt_sol["Capacity"].iloc[m] for m in range(len(check_prev)) if check_prev[m]]
                    # add all the capacities to find the bottom of the bar
                    opt_sol["Bottom {}".format(parameter)].iloc[j] = sum(list_of_prevs)



        if sep_axis:
            f, (ax) = plt.subplots(1, 3, sharey=False, figsize=(19, 6))
        else:
            f, (ax) = plt.subplots(1, 3, sharey=True, figsize=(19, 6))

        price_list = [" ${:}/m²".format(i) for i in price]
        prices = ','.join(price_list)
        f.suptitle("{} building with{}".format(building_name,prices))

        ax[0].set_ylabel('Installed Solar Panels Capacity (m²)')

        for j, frame in enumerate(['Azimuth','Tilt','GCR']):
            x_label_s = {'Azimuth':'Azimuth (degree)',
                         'Tilt':'Tilt (degree)',
                         'GCR':'Ground Coverage Ratio (-)'}
            bar_width = {'Azimuth':5,
                         'Tilt':5,
                         'GCR':0.05}
            x_lim_sets = {'Azimuth':[90,271],
                         'Tilt':[0,61],
                         'GCR':[0,1.01]}
            x_ticks_sets = {'Azimuth':30,
                         'Tilt':5,
                         'GCR':0.1}
            ax[j].set_xlim(x_lim_sets[frame])
            ax[j].set_xticks( np.arange(x_lim_sets[frame][0], x_lim_sets[frame][1], x_ticks_sets[frame]))
            ax[j].set_xlabel(x_label_s[frame])
            bins_first = np.arange(pv_panel_df[frame].min(),pv_panel_df[frame].max(),bar_width[frame]) + bar_width[frame]

            lists_of_lengeds0 = []
            if hista:
                for i in price:
                    panels_at_price = pv_panel_df.loc[(pv_panel_df["price"]==i)]
                    ax[j].hist(panels_at_price[frame],
                               weights=panels_at_price["Capacity"],
                               bins=bins_first,
                               histtype="step",align="right",
                              label="All Solutions ${}".format(i))
                lists_of_lengeds0 = ["All Solutions ${}".format(price) for price in price]

            for i,val in enumerate(price):
                optimalpvpanels=opt_sol[opt_sol["price"]==val]
                for panel_id in optimalpvpanels.index:
                    panel=optimalpvpanels.loc[panel_id]
                    ax[j].bar(panel[frame],
                              panel["Capacity"],
                              width=bar_width[frame],alpha=0.5,
                              bottom=panel["Bottom {}".format(frame)],
                              color="C{}".format(i),
                              label="Cheapest Solution  ${}".format(val))

            handles, labels = ax[j].get_legend_handles_labels()


        legend_df = pd.DataFrame(labels, columns=["labels"])
        legend_df["handles"] = handles
        legend_df=legend_df.drop_duplicates(subset="labels",keep="first")
        unique_labels = np.unique(labels)
        ax[2].legend(legend_df["handles"],legend_df["labels"],loc="upper left")


    def what_is_found_by_ga(self,building_name,location,Obj,price,sep_axis=False,hista=True,Run="45"):
        """
        This function goes and finds what is found by the GA on the pareto front. It then looks at
        what is installed and tallies up the angle, tilt and gcr of all the solutions to make a
        density plot. We also show were the cheapest option is found.

        args:
            building_name
            location
            Obj
            price
            sep_axis
            hista
        returns:
            A nice plot, what is found in the last population in by the GA.
        """


        ###############
        # Select the data we wish to plot
        self.selected_df = self.processed_df.loc[\
                                (self.processed_df["Building Name"]==building_name) &\
                                (self.processed_df["Location"]==location) & \
                                (self.processed_df["Run"]==Run) & \
                                (self.processed_df["Obj"]==Obj)]
        # To build the density plot we will construct a pv_panel_list
        # This will contain all panels found for the supplied metrics.
        # We also include total cost of the system to highlight the
        # location of the cheapest solution in the density plot.
        col_names = ["Capacity","Azimuth","Tilt","GCR","Total_cost","price"]
        pv_panel_df = pd.DataFrame(columns=col_names)
        # Find all the runs
        for i in range(len(self.selected_df)):
            df = pd.read_excel(io=self.selected_df["Filename"].iloc[i])
            # Each run we will open the processed excel file.
            for j in range(len(df)):
                # Each row we will find the panels that are installed and
                # add them to the pv_panel_list
                # Find the number of panels

                colnames_=list(df.columns)
                azi_names=[name_ for name_ in colnames_ if name_[:3]=="azi"]

                for k in range(len(azi_names)):
                    row_panel = []
                    row_panel.append(df["cap{}".format(k)].iloc[j])
                    row_panel.append(df["azi{}".format(k)].iloc[j])
                    row_panel.append(df["tilt{}".format(k)].iloc[j])
                    row_panel.append(df["gcr{}".format(k)].iloc[j])
                    row_panel.append(df["total_cost"].iloc[j])
                    row_panel.append(self.selected_df["Price"].iloc[i])
                    row_panel_df=pd.DataFrame([row_panel],columns=col_names)
                    pv_panel_df=pv_panel_df.append(row_panel_df,ignore_index=True)

        # Simple data to display in various forms
        opt_sol=pd.DataFrame([],columns=pv_panel_df.columns)
        for i, val in enumerate(price):
            panels_at_price = pv_panel_df.loc[pv_panel_df["price"]==val]

            # try to find the minimal solution
            # There might not be a minimal solution
            id_min = panels_at_price["Total_cost"].loc[
                panels_at_price["Capacity"]>=0] == panels_at_price["Total_cost"].min()
            cheapest_panel_row=panels_at_price.loc[id_min]
            opt_sol=opt_sol.append(cheapest_panel_row,ignore_index=True)
        # This will make sure the bars are stacked on top of each other by determing if
        # there is a bottom value of the bar. This bottom value is based on the if a
        # same setting is found at a higher price. If there is lower price with the same
        # setting we will stack this on top of the other bar by setting the bottom
        # variable.
        for i, parameter in enumerate(['Azimuth','Tilt','GCR']):
            # By default the bottom will be set to zero
            opt_sol["Bottom {}".format(parameter)] = 0
            # Then check the solutions in order of occurance
            for j,sol in enumerate(opt_sol[parameter]):
                # are there any solutions before using the same setting for the parameter
                arethereanybefore = any(opt_sol[parameter].iloc[0:j]==sol)
                # there are:
                if arethereanybefore:
                    # Check all the previous solutions to find those with the same setting
                    check_prev    = opt_sol[parameter].iloc[0:j]==sol
                    # Loop through all those, and build a list of them.
                    list_of_prevs = [opt_sol["Capacity"].iloc[m] for m in range(len(check_prev)) if check_prev[m]]
                    # add all the capacities to find the bottom of the bar
                    opt_sol["Bottom {}".format(parameter)].iloc[j] = sum(list_of_prevs)



        if sep_axis:
            f, (ax) = plt.subplots(1, 3, sharey=False, figsize=(19, 6))
        else:
            f, (ax) = plt.subplots(1, 3, sharey=True, figsize=(19, 6))

        price_list = [" ${:}/m²".format(i) for i in price]
        prices = ','.join(price_list)
        f.suptitle("{} building with{}".format(building_name,prices))

        ax[0].set_ylabel('Installed Solar Panels Capacity (m²)')

        for j, frame in enumerate(['Azimuth','Tilt','GCR']):
            x_label_s = {'Azimuth':'Azimuth (degree)',
                         'Tilt':'Tilt (degree)',
                         'GCR':'Ground Coverage Ratio (-)'}
            bar_width = {'Azimuth':5,
                         'Tilt':5,
                         'GCR':0.05}
            x_lim_sets = {'Azimuth':[90,271],
                         'Tilt':[0,61],
                         'GCR':[0,1.01]}
            x_ticks_sets = {'Azimuth':30,
                         'Tilt':5,
                         'GCR':0.1}
            ax[j].set_xlim(x_lim_sets[frame])
            ax[j].set_xticks( np.arange(x_lim_sets[frame][0], x_lim_sets[frame][1], x_ticks_sets[frame]))
            ax[j].set_xlabel(x_label_s[frame])
            bins_first = np.arange(pv_panel_df[frame].min(),pv_panel_df[frame].max(),bar_width[frame]) + bar_width[frame]

            lists_of_lengeds0 = []
            if hista:
                for i in price:
                    panels_at_price = pv_panel_df.loc[(pv_panel_df["price"]==i)]
                    ax[j].hist(panels_at_price[frame],
                               weights=panels_at_price["Capacity"],
                               bins=bins_first,
                               histtype="step",align="right",
                              label="All Solutions ${}".format(i))
                lists_of_lengeds0 = ["All Solutions ${}".format(price) for price in price]

            for i,val in enumerate(price):
                optimalpvpanels=opt_sol[opt_sol["price"]==val]
                for panel_id in optimalpvpanels.index:
                    panel=optimalpvpanels.loc[panel_id]
                    ax[j].bar(panel[frame],
                              panel["Capacity"],
                              width=bar_width[frame],alpha=0.5,
                              bottom=panel["Bottom {}".format(frame)],
                              color="C{}".format(i),
                              label="Cheapest Solution  ${}".format(val))

            handles, labels = ax[j].get_legend_handles_labels()


        legend_df = pd.DataFrame(labels, columns=["labels"])
        legend_df["handles"] = handles
        legend_df=legend_df.drop_duplicates(subset="labels",keep="first")
        unique_labels = np.unique(labels)
        ax[2].legend(legend_df["handles"],legend_df["labels"],loc="upper left")



        #ax[2].legend(list(flatten([lists_of_lengeds0,lists_of_lengeds1])),loc="upper left")
    def configs_fused(self,building_name,location,Obj,price,sep_axis=False,hista=True,Run="45"):
        """
        This function goes and finds what is found by the GA on the pareto front. It then looks at
        what is installed and tallies up the angle, tilt and gcr of all the solutions to make a
        density plot. We also show were the cheapest option is found.

        args:
            building_name
            location
            Obj
            price
            sep_axis
            hista
        returns:
            A nice plot, what is found in the last population in by the GA.
        """


        ###############
        # Select the data we wish to plot
        self.selected_df = self.processed_df.loc[\
                                (self.processed_df["Building Name"]==building_name) &\
                                (self.processed_df["Location"]==location) & \
                                (self.processed_df["Run"]==Run) & \
                                (self.processed_df["Obj"]==Obj)]
        # To build the density plot we will construct a pv_panel_list
        # This will contain all panels found for the supplied metrics.
        # We also include total cost of the system to highlight the
        # location of the cheapest solution in the density plot.
        col_names = ["Capacity","Azimuth","Tilt","GCR","Total_cost","Energy Imported","price"]
        pv_panel_df = pd.DataFrame(columns=col_names)
        # Find all the runs
        for i in range(len(self.selected_df)):
            df = pd.read_excel(io=self.selected_df["Filename"].iloc[i])
            # Each run we will open the processed excel file.
            for j in range(len(df)):
                # Each row we will find the panels that are installed and
                # add them to the pv_panel_list
                # Find the number of panels

                colnames_=list(df.columns)
                azi_names=[name_ for name_ in colnames_ if name_[:3]=="azi"]

                for k in range(len(azi_names)):
                    row_panel = []
                    row_panel.append(df["cap{}".format(k)].iloc[j])
                    row_panel.append(df["azi{}".format(k)].iloc[j])
                    row_panel.append(df["tilt{}".format(k)].iloc[j])
                    row_panel.append(df["gcr{}".format(k)].iloc[j])
                    row_panel.append(df["total_cost"].iloc[j])
                    row_panel.append(df["Energy Imported"].iloc[j])
                    row_panel.append(self.selected_df["Price"].iloc[i])
                    row_panel_df=pd.DataFrame([row_panel],columns=col_names)
                    pv_panel_df=pv_panel_df.append(row_panel_df,ignore_index=True)

        # Simple data to display in various forms
        opt_sol=pd.DataFrame([],columns=pv_panel_df.columns)
        for i, val in enumerate(price):
            panels_at_price = pv_panel_df.loc[pv_panel_df["price"]==val]

            # try to find the minimal solution
            # There might not be a minimal solution
            min_import =  panels_at_price["Energy Imported"].min()
            id_min = panels_at_price[panels_at_price["Capacity"]>=0]["Energy Imported"] == min_import
            cheapest_panel_row=panels_at_price.loc[id_min]
            cheapest_panel_row.loc[:,'option'] = 'energy'
            opt_sol=opt_sol.append(cheapest_panel_row,ignore_index=True)

        # Simple data to display in various forms
        for i, val in enumerate(price):
            panels_at_price = pv_panel_df.loc[pv_panel_df["price"]==val]

            # try to find the minimal solution
            # There might not be a minimal solution
            
            id_min = panels_at_price[panels_at_price["Capacity"]>=0]["Total_cost"] == panels_at_price["Total_cost"].min()
            cheapest_panel_row=panels_at_price.loc[id_min]
            cheapest_panel_row.loc[:,'option'] = 'cost'
            opt_sol=opt_sol.append(cheapest_panel_row,ignore_index=True)
        def myround(x, base=0.05):
            return base * round(x/base)
        
        bar_width = {'Azimuth':5,
                    'Tilt':1,
                    'GCR':0.05}
        
        opt_sol['GCR'] = myround(opt_sol['GCR'],base=bar_width['GCR'])
        opt_sol['Tilt'] = myround(opt_sol['Tilt'],base=bar_width['Tilt'])
        opt_sol['Azimuth'] = myround(opt_sol['Azimuth'],base=bar_width['Azimuth'])

        # for the plot we don't need super accurate values, to show the right angle 
        # we want to round the azimuth to the nearst 5 degrees, tilt to the nearest 1 
        # degree and the gcr to the nearest 0.05



        # This will make sure the bars are stacked on top of each other by determing if
        # there is a bottom value of the bar. This bottom value is based on the if a
        # same setting is found at a higher price. If there is lower price with the same
        # setting we will stack this on top of the other bar by setting the bottom
        # variable.
        for i, parameter in enumerate(['Azimuth','Tilt','GCR']):
            # By default the bottom will be set to zero
            opt_sol["Bottom {}".format(parameter)] = 0
            # Then check the solutions in order of occurance
            for j,sol in enumerate(opt_sol[parameter]):
                # are there any solutions before using the same setting for the parameter
                arethereanybefore = any(opt_sol[parameter].iloc[0:j]==sol)
                # there are:
                if arethereanybefore:
                    # Check all the previous solutions to find those with the same setting
                    check_prev    = opt_sol[parameter].iloc[0:j]==sol
                    # Loop through all those, and build a list of them.
                    list_of_prevs = [opt_sol["Capacity"].iloc[m] for m in range(len(check_prev)) if check_prev[m]]
                    # add all the capacities to find the bottom of the bar
                    opt_sol["Bottom {}".format(parameter)].iloc[j] = sum(list_of_prevs)



        if sep_axis:
            f, (ax) = plt.subplots(1, 3, sharey=False, figsize=(19, 6))
        else:
            f, (ax) = plt.subplots(1, 3, sharey=True, figsize=(19, 6))

        price_list = [" \${:}/m²".format(i) for i in price]
        prices = ','.join(price_list)
        f.suptitle("{} building with{}".format(building_name,prices))

        ax[0].set_ylabel('Installed Solar Panels Capacity (m²)')

        for j, frame in enumerate(['Azimuth','Tilt','GCR']):
            x_label_s = {'Azimuth':'Azimuth (degree)',
                         'Tilt':'Tilt (degree)',
                         'GCR':'Ground Coverage Ratio (-)'}
            x_lim_sets = {'Azimuth':[90,271],
                         'Tilt':[0,61],
                         'GCR':[0,1.01]}
            x_ticks_sets = {'Azimuth':30,
                         'Tilt':5,
                         'GCR':0.1}
            ax[j].set_xlim(x_lim_sets[frame])
            ax[j].set_xticks( np.arange(x_lim_sets[frame][0], x_lim_sets[frame][1], x_ticks_sets[frame]))
            ax[j].set_xlabel(x_label_s[frame])
            bins_first = np.arange(pv_panel_df[frame].min(),pv_panel_df[frame].max(),bar_width[frame]) + bar_width[frame]

            lists_of_lengeds0 = []
            if hista:
                for i in price:
                    panels_at_price = pv_panel_df.loc[(pv_panel_df["price"]==i)]
                    ax[j].hist(panels_at_price[frame],
                               weights=panels_at_price["Capacity"],
                               bins=bins_first,
                               histtype="step",align="right",
                               label="All Solutions ${}".format(i))
                lists_of_lengeds0 = ["All Solutions ${}".format(price) for price in price]

            for i,val in enumerate(price):
                optimalpvpanels=opt_sol[opt_sol["price"]==val]
                for panel_id in optimalpvpanels.index:
                    panel=optimalpvpanels.loc[panel_id]
                    option = panel['option']
                    if option=='cost':
                        ax[j].bar(panel[frame],
                              panel["Capacity"],
                              width=bar_width[frame],alpha=0.5,
                              bottom=panel["Bottom {}".format(frame)],
                              color="C{}".format(i),
                              hatch="\\",
                              edgecolor='black',
                              label="Best Cost ${}".format(val))
                    else:
                        ax[j].bar(panel[frame],
                              panel["Capacity"],
                              width=bar_width[frame],alpha=0.5,
                              bottom=panel["Bottom {}".format(frame)],
                              color="C{}".format(i),
                              edgecolor='black',
                              hatch=".",
                              label="Lowest Grid Imports ${}".format(val))

            handles, labels = ax[j].get_legend_handles_labels()


        legend_df = pd.DataFrame(labels, columns=["labels"])
        legend_df["handles"] = handles
        legend_df=legend_df.drop_duplicates(subset="labels",keep="first")
        unique_labels = np.unique(labels)
        ax[2].legend(legend_df["handles"],legend_df["labels"],loc="upper left")
        return ax
    
    
    def configs_fused_single(self,building_name,location,Obj,price,sep_axis=False,hista=True,Run="45",ax=None, 
                             legend_toggle=False,max_y_val=1500,fig=None,turn_on_xaxis_title=True,pre_emble=None):
        """
        This function goes and finds what is found by the GA on the pareto front. It then looks at
        what is installed and tallies up the angle, tilt and gcr of all the solutions to make a
        density plot. We also show were the cheapest option is found.

        args:
            building_name
            location
            Obj
            price
            sep_axis
            hista
        returns:
            A nice plot, what is found in the last population in by the GA.
        """


        ###############
        # Select the data we wish to plot
        self.selected_df = self.processed_df.loc[\
                                (self.processed_df["Building Name"]==building_name) &\
                                (self.processed_df["Location"]==location) & \
                                (self.processed_df["Run"]==Run) & \
                                (self.processed_df["Obj"]==Obj)]
        # To build the density plot we will construct a pv_panel_list
        # This will contain all panels found for the supplied metrics.
        # We also include total cost of the system to highlight the
        # location of the cheapest solution in the density plot.
        col_names = ["Capacity","Azimuth","Tilt","GCR","Total_cost","Energy Imported","price"]
        pv_panel_df = pd.DataFrame(columns=col_names)
        # Find all the runs
        for i in range(len(self.selected_df)):
            df = pd.read_excel(io=self.selected_df["Filename"].iloc[i])
            # Each run we will open the processed excel file.
            for j in range(len(df)):
                # Each row we will find the panels that are installed and
                # add them to the pv_panel_list
                # Find the number of panels

                colnames_=list(df.columns)
                azi_names=[name_ for name_ in colnames_ if name_[:3]=="azi"]

                for k in range(len(azi_names)):
                    row_panel = []
                    row_panel.append(df["cap{}".format(k)].iloc[j])
                    row_panel.append(df["azi{}".format(k)].iloc[j])
                    row_panel.append(df["tilt{}".format(k)].iloc[j])
                    row_panel.append(df["gcr{}".format(k)].iloc[j])
                    row_panel.append(df["total_cost"].iloc[j])
                    row_panel.append(df["Energy Imported"].iloc[j])
                    row_panel.append(self.selected_df["Price"].iloc[i])
                    row_panel_df=pd.DataFrame([row_panel],columns=col_names)
                    pv_panel_df=pv_panel_df.append(row_panel_df,ignore_index=True)

        # Simple data to display in various forms
        opt_sol=pd.DataFrame([],columns=pv_panel_df.columns)
        for i, val in enumerate(price):
            panels_at_price = pv_panel_df.loc[pv_panel_df["price"]==val]

            # try to find the minimal solution
            # There might not be a minimal solution
            min_import =  panels_at_price["Energy Imported"].min()
            id_min = panels_at_price[panels_at_price["Capacity"]>=0]["Energy Imported"] == min_import
            cheapest_panel_row=panels_at_price.loc[id_min]
            cheapest_panel_row.loc[:,'option'] = 'energy'
            opt_sol=opt_sol.append(cheapest_panel_row,ignore_index=True)

        # Simple data to display in various forms
        for i, val in enumerate(price):
            panels_at_price = pv_panel_df.loc[pv_panel_df["price"]==val]

            # try to find the minimal solution
            # There might not be a minimal solution
            
            id_min = panels_at_price[panels_at_price["Capacity"]>=0]["Total_cost"] == panels_at_price["Total_cost"].min()
            cheapest_panel_row=panels_at_price.loc[id_min]
            cheapest_panel_row.loc[:,'option'] = 'cost'
            opt_sol=opt_sol.append(cheapest_panel_row,ignore_index=True)
        def myround(x, base=0.05):
            return base * round(x/base)
        
        bar_width = {'Azimuth':5,
                    'Tilt':1,
                    'GCR':0.05}
        
        opt_sol['GCR'] = myround(opt_sol['GCR'],base=bar_width['GCR'])
        opt_sol['Tilt'] = myround(opt_sol['Tilt'],base=bar_width['Tilt'])
        opt_sol['Azimuth'] = myround(opt_sol['Azimuth'],base=bar_width['Azimuth'])

        # for the plot we don't need super accurate values, to show the right angle 
        # we want to round the azimuth to the nearst 5 degrees, tilt to the nearest 1 
        # degree and the gcr to the nearest 0.05



        # This will make sure the bars are stacked on top of each other by determing if
        # there is a bottom value of the bar. This bottom value is based on the if a
        # same setting is found at a higher price. If there is lower price with the same
        # setting we will stack this on top of the other bar by setting the bottom
        # variable.
        for i, parameter in enumerate(['Azimuth','Tilt','GCR']):
            # By default the bottom will be set to zero
            opt_sol["Bottom {}".format(parameter)] = 0
            # Then check the solutions in order of occurance
            for j,sol in enumerate(opt_sol[parameter]):
                # are there any solutions before using the same setting for the parameter
                arethereanybefore = any(opt_sol[parameter].iloc[0:j]==sol)
                # there are:
                if arethereanybefore:
                    # Check all the previous solutions to find those with the same setting
                    check_prev    = opt_sol[parameter].iloc[0:j]==sol
                    # Loop through all those, and build a list of them.
                    list_of_prevs = [opt_sol["Capacity"].iloc[m] for m in range(len(check_prev)) if check_prev[m]]
                    # add all the capacities to find the bottom of the bar
                    opt_sol["Bottom {}".format(parameter)].iloc[j] = sum(list_of_prevs)



#         if sep_axis:
#             f, (ax) = plt.subplots(1, 3, sharey=False, figsize=(19, 6))
#         else:
#             f, (ax) = plt.subplots(1, 3, sharey=True, figsize=(19, 6))
        font = {'family' : 'Arial',
                'weight' : 'bold',
                'size'   : 20}  
    
        price_list = [" \${:}/m²".format(i) for i in price]
        prices = ','.join(price_list)
        ax[1].set_title("{} building with{}".format(pre_emble,prices),fontdict=font)
        ax[1].title.set_position([.5, 1.1])
        ax[0].set_ylabel('Installed PV\n Panel Area (m²)',fontdict=font)
        ax[1].set(yticklabels=[])
        ax[2].set(yticklabels=[])
        
        for j, frame in enumerate(['Azimuth','Tilt','GCR']):
            ax[j].set_ylim((0,max_y_val))
            
            x_label_s = {'Azimuth':'Azimuth (degree)',
                         'Tilt':'Tilt (degree)',
                         'GCR':'Ground Coverage Ratio (-)'}
            anno_x = {'Azimuth':92,
                         'Tilt':1,
                         'GCR':0.02}
            anno_l = {'Azimuth':f'({pre_emble[1]}-I)',
                         'Tilt':f'({pre_emble[1]}-II)',
                         'GCR':f'({pre_emble[1]}-III)'}
            ax[j].annotate(anno_l[frame],(anno_x[frame],max_y_val-max_y_val*0.1))
            
            x_lim_sets = {'Azimuth':[90,271],
                         'Tilt':[0,61],
                         'GCR':[0,1.01]}
            x_ticks_sets = {'Azimuth':30,
                         'Tilt':5,
                         'GCR':0.1}
            
            ax[j].set_xlim(x_lim_sets[frame])
            ax[j].set_xticks( np.arange(x_lim_sets[frame][0], x_lim_sets[frame][1], x_ticks_sets[frame]))
            if turn_on_xaxis_title:
                ax[j].set_xlabel(x_label_s[frame],fontdict=font)
            bins_first = np.arange(pv_panel_df[frame].min(),pv_panel_df[frame].max(),bar_width[frame]) + bar_width[frame]

            lists_of_lengeds0 = []
            if hista:
                for i in price:
                    panels_at_price = pv_panel_df.loc[(pv_panel_df["price"]==i)]
                    ax[j].hist(panels_at_price[frame],
                               weights=panels_at_price["Capacity"],
                               bins=bins_first,
                               histtype="step",align="right",
                               label="All Solutions ${}".format(i))
                lists_of_lengeds0 = ["All Solutions ${}".format(price) for price in price]

            for i,val in enumerate(price):
                optimalpvpanels=opt_sol[opt_sol["price"]==val]
                for panel_id in optimalpvpanels.index:
                    panel=optimalpvpanels.loc[panel_id]
                    option = panel['option']
                    if option=='cost':
                        ax[j].bar(panel[frame],
                              panel["Capacity"],
                              width=bar_width[frame],alpha=0.5,
                              bottom=panel["Bottom {}".format(frame)],
                              color="C{}".format(i),
                              hatch="\\",
                              edgecolor='black',
                              label="Min. Cost ${}".format(val))
                    else:
                        ax[j].bar(panel[frame],
                              panel["Capacity"],
                              width=bar_width[frame],alpha=0.5,
                              bottom=panel["Bottom {}".format(frame)],
                              color="C{}".format(i),
                              edgecolor='black',
                              hatch=".",
                              label="Min. Grid Imports ${}".format(val))

            handles, labels = ax[j].get_legend_handles_labels()

        if legend_toggle:
            font_leg = {'family' : 'Arial',
                'weight' : 'bold',
                'size'   : 16}  
            legend_df = pd.DataFrame(labels, columns=["labels"])
            legend_df["handles"] = handles
            legend_df=legend_df.drop_duplicates(subset="labels",keep="first")
            unique_labels = np.unique(labels)
            fig.legend(legend_df["handles"],legend_df["labels"],  
                         loc= 'upper right', shadow=False, ncol=1, mode=None,framealpha=1,prop=font_leg)
        return ax, fig