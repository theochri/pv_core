import numpy as np
import pandas as pd
from pandas.core.common import flatten
from besos.parameters import CategoryParameter, RangeParameter, Parameter
from besos.problem    import Problem
from besos.evaluator  import EvaluatorSR
from besos.optimizer  import NSGAII, rbf_opt
from platypus import Archive
from pv_model import pvModel
import os

import logging
logger = logging.getLogger()
logger.setLevel(logging.ERROR)

lgg       = logging.getLogger('optimizer_logger')
hdlr      = logging.FileHandler('optimizer_logger.txt')
formatter = logging.Formatter('%(asctime)s %(message)s')
hdlr.setFormatter(formatter)
lgg.addHandler(hdlr) 
lgg.setLevel(logging.ERROR)

from platypus import  TerminationCondition

class MaxEvaluations(TerminationCondition):
    """Termination condition based on the maximum number of function evaluations.
    
    Note that since we check the termination condition after each iteration, it
    is possible for the algorithm to exceed the max NFE.
    
    Parameters
    ----------
    nfe : int
        The maximum number of function evaluations to execute.
    """
    def __init__(self, nfe, _parameterset):
        super(MaxEvaluations, self).__init__()
        self.nfe = nfe
        self._parameterset = _parameterset
        self.starting_nfe = 0
        
    def initialize(self, algorithm):
        self.starting_nfe = algorithm.nfe
        
    def shouldTerminate(self, algorithm):
        lgg.error(algorithm.nfe)
        lgg.error(self._parameterset)
        return algorithm.nfe - self.starting_nfe >= self.nfe
        


class PvOptimizer:
    
    def __init__(self,
                 range_azi,
                 range_tilt,
                 range_gcr,
                 pvpanel,
                 price,
                 store_file,
                 ehub_request,
                 res_file,
                 pvbuilding,
                 roof_area_per_unit_capacity,
                 ga_proc,
                 solver_settings,
                 **kwargs,):
        '''
        Setup the optmizer
        param: range_azi    - Range of azimuths allowed by the optimizer.
        param: range_tilt   - Range of tilts allowed by the optimizer.
        param: range_gcr    - Range of Ground Coverage Ratios allowed by the optimizer.
        param: pvpanel      - the pvFactors object (see pvPanel)
        param: price        - the price of each panel
        param: store_file   - Where do you want to store the GA results
        param: ehub_request - Preloaded EHub excel file
        param: roof_area    - What is the roof area of the building
        param: res_file     - Where do you want to store the single results file from the ehub run
        param: hourly_electricity - Hourly electricity of the building
        param: roof_area_per_unit_capacity - Roof area per unit capacity
        param: ga_proc      - The processed file.
        optional param: solver_settings - Some settings to set the cluster
        '''
        self.range_azi  = range_azi
        self.range_tilt = range_tilt
        self.range_gcr  = range_gcr
        self.pvpanel    = pvpanel
        self.price      = price
        self.store_file = store_file
        self.ensure_dir(store_file)
        
        self._pvbuilding= pvbuilding
        self._request   = ehub_request
        self._AREA      = self._pvbuilding.roof_area
        self._File      = res_file
        self.ga_proc    = ga_proc
        self._LOADS     = self._pvbuilding.hourly_electricity
        self._AREA_CAP  = roof_area_per_unit_capacity
        
        
        # Build the various Descriptors
        cat=False
        if cat:
            azi  = CategoryParameter(options=range_azi)
            tilt = CategoryParameter(options=range_tilt)
            gcr  = CategoryParameter(options=range_gcr)
        else:
            azi  = RangeParameter(min_val=min(range_azi),max_val=max(range_azi))
            tilt = RangeParameter(min_val=min(range_tilt),max_val=max(range_tilt))
            gcr  = RangeParameter(min_val=min(range_gcr),max_val=max(range_gcr))
        self.num_panels=3
        # Build the list of all the GA Selection Parameters is the orientation of the panel x (num_panels)
        self.parameters = list(flatten([
            [Parameter(value_descriptor=azi, name='azi{}'.format(i)) for i in range(self.num_panels)],
            [Parameter(value_descriptor=tilt, name='tilt{}'.format(i)) for i in range(self.num_panels)],
            [Parameter(value_descriptor=gcr, name='gcr{}'.format(i)) for i in range(self.num_panels)]
                     ]
        ))
        try:
            self.solver_settings = solver_settings
            lgg.error("Found the solver set")
        except:
            lgg.error("Did not find the solver set")
            self.solver_settings = {
                    'name': 'glpk',
                    'options': {
                                'mipgap': 0.05,
                               },
                    }

    def print_permulations(self):
        '''
        Calculate & print the total permutations
        '''
        try:
            num_permutations = len(self.range_azi)*len(self.range_tilt)*len(self.range_gcr)
        except:
            num_permutations = "inf"
        print(num_permutations)
        
    def runGA(self,num_evaluations,size_population,model="BC",obj="SINGLE"):
        self._economic_model=model
        self._objective_model=obj
        self.set_problem_and_evaluation()
        _parameterset = {"building_idf":self._pvbuilding.idf_file,
                         "price":self.price}
        lgg.error(f"Started {_parameterset}")
        stop_evals = MaxEvaluations(num_evaluations,_parameterset) # We added this to log where things are going
        # Run the optimizer using this evaluator for a population size of 20 for 10 generations
        self.ga_results = NSGAII(self.eva_ga, 
                                 evaluations=stop_evals, 
                                 population_size=size_population) 
        self.ga_results.to_excel(self.store_file,index=False)
        lgg.error(f"Finished and stored: {_parameterset}")

    def runSWARM(self,num_evaluations,size_population,model="BC",obj="SINGLE"):
        self._economic_model=model
        self._objective_model=obj
        self.set_problem_and_evaluation()
        _parameterset = {"building_idf":self._pvbuilding.idf_file,
                         "price":self.price}
        lgg.error(f"Started {_parameterset}")
        stop_evals = MaxEvaluations(num_evaluations,_parameterset) 
        # We added this to log where things are going
        # Run the optimizer using this evaluator for a population size of 20 for 10 generations
        self.ga_results = ParticleSwarm(self.eva_ga, 
                                 evaluations=stop_evals, 
                                 swarm_size=size_population,
                                 leader_size=size_population) 
        self.ga_results.to_excel(self.store_file,index=False)
        lgg.error(f"Finished and stored: {_parameterset}")   
        
    def runRBFOPT(self,num_evaluations,model="BC",obj="SINGLE"):
        self._economic_model=model
        self._objective_model=obj
        self.set_problem_and_evaluation()
        _parameterset = {"building_idf":self._pvbuilding.idf_file,
                         "price":self.price}
        lgg.error(f"The number of evaluations is {num_evaluations}")
        self.ga_results = rbf_opt(self.eva_ga,num_evaluations) 
        self.ga_results.to_excel(self.store_file,index=False)
        lgg.error(f"Finished and stored: {_parameterset}")
    
    def set_problem_and_evaluation(self):
        # Build up the problem
        
        if (self._objective_model=="SINGLE"):
            # Here we have a simple total cost reduction
            problem_to_be_solved = Problem(self.parameters,\
                                                outputs=['totalcost'],\
                                                minimize_outputs=[True])
            eva_fun = self.eval_single
        elif (self._objective_model=="SINGLE-NET"):
            # Here we have a simple total cost reduction
            problem_to_be_solved = Problem(self.parameters,\
                                                outputs=['Net-Electricity'],\
                                                minimize_outputs=[True])
            eva_fun = self.eval_single_net
        elif (self._objective_model=="DUAL"):
            # here we pursue both minimal cost and minimize net-consumption of the grid
            problem_to_be_solved = Problem(self.parameters,\
                                                outputs=['totalcost','Net-Electricity'],\
                                                minimize_outputs=[True, True])
            eva_fun = self.eval_dual
        else:
            print("Incorrectly set objective, \
                   set SINGLE to minimize cost \
                   set DUAL to minimize cost and maximize capacity")
        self.eva_ga = EvaluatorSR(eva_fun, problem_to_be_solved)

    def eval_single(self,values):
        """
        This is the evaluation function that returns the total_cost
        of the optmized system
        """
        my_model = self.optimizeModel(values)
        # This basically stores al the information we are interested in beyond the objectives.
        self.row_to_excel(my_model)
        # Pull the cost of the model
        cost = my_model.totCost
        return (cost,), ()
    
    def eval_single_net(self,values):
        """
        This is the evaluation function that returns the total_cost
        of the optmized system
        """
        my_model = self.optimizeModel(values)
        # This basically stores al the information we are interested in beyond the objectives.
        self.row_to_excel(my_model)
        # Pull the cost of the model
        eng  = my_model.energyIm # Net energy imported
        return (eng,), ()

    def eval_dual(self,values):
        """
        This is the evaluation function that returns the total_cost
        and the total installed capacity of the optimized system
        """
        my_model = self.optimizeModel(values)
        # This basically stores al the information we are interested in beyond the objectives.
        self.row_to_excel(my_model)
        # Grab the 
        eng  = my_model.energyIm # Net energy imported
        cost = my_model.totCost  # Total cost of the system
        return (cost,eng,), ()
    
    def row_to_excel(self,my_model):
        """
        write extra content to excel
        """
        df_row = self.model_to_process_df_row(my_model)
        try:
            # Try to append
            temp = pd.read_excel(self.ga_proc)
            temp = temp.append(df_row, ignore_index=False)
            temp.to_excel(self.ga_proc,index=False)
        except:
            # if fail, then just make the first file.
            df_row.to_excel(self.ga_proc,index=False)
        
   
    def optimizeModel(self,values):
        """
        This function builds and evaluates a my_model object 
        that runs either the BC of ON model. 
        """
        try:
            # This try loop catches any errors. 
            try:
                # Try the run the model
                if (self._economic_model=="BC"):
                    my_model = pvModel.ModelBC(request=self._request,
                                               pvbuilding=self._pvbuilding,
                                               ROOF_AREA_CAP=self._AREA_CAP,
                                               resultsFile=self._File,
                                               NUM_PANELS=3,
                                               solver_settings=self.solver_settings)
                    my_model.eval_model(values,self.price,self.pvpanel)
                    print("succes")
                elif (self._economic_model=="DUMMY"):
                    lgg.error("DUMMY MODEL ENGAGED")
                    lgg.error(self.price)
                    my_model = pvModel.ModelBC(request=self._request,
                                               pvbuilding=self._pvbuilding,
                                               ROOF_AREA_CAP=self._AREA_CAP,
                                               resultsFile=self._File,
                                               NUM_PANELS=3,
                                               solver_settings=self.solver_settings)
                    my_model.energyIm = sum(values) # Set some stupid high value
                    my_model.totCost  = sum(values) # Set some stupid high value
                    my_model.totCapacity    = 0
                    my_model.panelCap       = [0,0,0]
                    my_model.costBreak      = {'Levelized Costs':0,
                                              'Energy Charge':10000000000,
                                              'Demand Charge':0,
                                              'Daily Charge' :0}
                    my_model.energyProd     = 0
                    my_model.roof_area_used = 0
                    my_model.Orientations   = values 
                elif (self._economic_model=="ON"):
                    my_model = pvModel.ModelON(request=self._request,\
                                           MAX_ROOF_AREA = self._AREA,\
                                           resultsFile   = self._File,\
                                           EP_LOAD       = self._LOADS,\
                                           ROOF_AREA_CAP = self._AREA_CAP,\
                                           NUM_PANELS    = self.num_panels)
                    my_model.eval_model(values,self.price,self.pvpanel)   
                else:
                    print("Incorrectly set model, \
                           set BC to set the Vancouver model \
                           set ON to set the Toronto model")
            except IndexError:
                # This error is caused by a too small factor_multiplier
                # We increase it by two to 3. This sometimes solves the 
                # issue. 
                print("IndexError catch")
                if (self._economic_model=="BC"):
                    my_model = pvModel.ModelBC(request=self._request,
                                               pvbuilding=self._pvbuilding,
                                               ROOF_AREA_CAP=self._AREA_CAP,
                                               resultsFile=self._File,
                                               NUM_PANELS=3,
                                               factor_multiplier=3,
                                               solver_settings=self.solver_settings)
                    my_model.eval_model(values,self.price,self.pvpanel)
                else:
                    print("only works on BC model")
            except ZeroDivisionError:
                # This error is caused by a too large factor_multiplier, we 
                # change the factor_multiplier to 1.1
                print("ZeroDivisionError catch")
                if (self._economic_model=="BC"):
                    my_model = pvModel.ModelBC(request=self._request,
                                               pvbuilding=self._pvbuilding,
                                               ROOF_AREA_CAP=self._AREA_CAP,
                                               resultsFile=self._File,
                                               NUM_PANELS=3,
                                               factor_multiplier=1.1,
                                               solver_settings=self.solver_settings)
                    my_model.eval_model(values,self.price,self.pvpanel)
                else:
                    print("only works on BC model")
        except Exception as inst:
            print(type(inst))    # the exception instance
            print(inst.args)     # arguments stored in .args
            print(inst)          # __str__ allows args to be printed directly
            # If we have not solved the problem using these two tricks
            # set the value to something bs, that will not be looked at 
            # the optimizer.
            lgg.error("Something went unexpectly wrong with:")
            lgg.error(self.price)
            my_model.energyIm = 10000000000 # Set some stupid high value
            my_model.totCost  = 10000000000 # Set some stupid high value
            my_model.totCapacity    = 0
            my_model.panelCap       = [0,0,0]
            my_model.costBreak      = {'Levelized Costs':0,
                                      'Energy Charge':10000000000,
                                      'Demand Charge':0,
                                      'Daily Charge' :0}
            my_model.energyProd     = 0
            my_model.roof_area_used = 0
            my_model.Orientations   = values
        return my_model
    
    def ensure_dir(self,file_path):
        directory = os.path.dirname(file_path)
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        
    def processGA(self,ga_processed):
        self.ensure_dir(ga_processed)
        '''
        Improve processor which should work on both On and BC. 
        '''
        archive_loaded= pd.read_excel(self.ga_proc)
        results_loaded=pd.read_excel(self.store_file)
        result = pd.merge(results_loaded, archive_loaded, on=['azi0', 'azi1', 'azi2', 'tilt0', 'tilt1', 'tilt2', 'gcr0', 'gcr1','gcr2'])
        result.to_excel(ga_processed)
        
    def model_to_process_df_row(self,my_model):
        cost      = my_model.totCost
        cap       = my_model.totCapacity
        caps      = my_model.panelCap
        cost_break = my_model.costBreak
        energy_produced = my_model.energyProd 
        energy_imported = my_model.energyIm
        roof_area_used  = my_model.roof_area_used
        values = my_model.Orientations
        processList = self.build_df_from_model_eval(values, cost, cap,
                                                       caps,  
                                                       cost_break,
                                                       energy_produced,
                                                       energy_imported,
                                                       roof_area_used)
        return processList
    
    def build_df_from_model_eval(self,vals, cost, cap, caps, cost_break, energy_produced, energy_imported, roof_area_used):
        # Compose the list and put it into a matrix
        col_azi = ["azi{}".format(i) for i in range(self.num_panels)]
        col_tilt= ["tilt{}".format(i) for i in range(self.num_panels)]
        col_gcr = ["gcr{}".format(i) for i in range(self.num_panels)]
        col_cap = ["cap{}".format(i) for i in range(self.num_panels)]
        column_names = list(flatten([col_azi,col_tilt,col_gcr,\
                                     ["total_cost","total_cap"],\
                                     col_cap,\
                                    ["Cost Break",\
                                     "Panel Energy Produced",\
                                     "Energy Imported","Roof Area Used"]]))
        list1 = []
        list1.extend(vals)
        list1.append(cost)
        list1.append(cap)
        list1.extend(caps)
        list1.append(cost_break)
        list1.append(energy_produced)
        list1.append(energy_imported)
        list1.append(roof_area_used)
        list1=[list1]
        print(list1)
        # Put the matrix into the dataframe
        return pd.DataFrame(list1,columns=column_names)