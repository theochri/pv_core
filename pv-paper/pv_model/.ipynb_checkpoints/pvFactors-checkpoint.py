import numpy as np
import pandas as pd

import pvlib
import pickle

from pvfactors.geometry import OrderedPVArray
from pvfactors.run import run_parallel_engine
from pvfactors.engine import PVEngine

# Prevent pink things being printed.
import warnings
import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

class PvPanel:
    
    def __init__(self,rendered_files):
        '''
        By initiating the a panel we have set a couple of good parameters.
        '''
        self.pp = {
            'n_pvrows': 3,               # number of pv rows
            'pvrow_height': 2,           # height of pvrows (measured at center / torque tube)
            'pvrow_width': 1,            # width of pvrows [m]
            'pvrow_depth': 1,            # depth of the panel [m]
            'axis_azimuth': 0,           # azimuth angle of rotation axis
            'surface_tilt': 20.,         # tilt of the pv rows
            'surface_azimuth': 90.,      # azimuth of the pv rows front surface
            'solar_zenith': 60.,         # solar zenith angle
            'solar_azimuth': 180.,       # solar azimuth angle
            'gcr': 0.5,                  # ground coverage ratio
            'albedo': 0.2,               # ground surface reflectivity (used to be rho_ground)
            'rho_front_pvrow': 0.01,     # front surface reflectivity of PV rows
            'rho_back_pvrow': 0.01       # back surface reflectivity of PV rows
        }
        discretization = {'cut':{
            0: {'front': 5},  # discretize the back side of the leftmost PV row into 5 segments
            1: {'front': 5},  # discretize the front side of the center PV row into 5 segments
            2: {'front': 5}   # discretize the front side of the center PV row into 5 segments
        }}
        #self.pp.update(discretization)
        self.rendered_files=rendered_files
    
    def setup_array(self,pvbuilding):
        '''
        Setup the panel for a particular building
        param: pvbuilding - a pvbuilding object
        
        1) Make sure to setup the weather from the EPW file
        2) Pull the weather from the building save locally
        3) Offset the array with the building north axis 
        4) Innitialize an ordered PV array from pvfactors        
        '''
        pvbuilding.get_weather(2019)
        self.weather = pvbuilding.weather
        self.meta    = pvbuilding.meta
        
        # Setup the north axis and panel height to match the top of the building
        # these features are turned off for now.
        # self.set_north_axis(pvbuilding.North_Axis)  
        # self.set_panel_height(pvbuilding.roof_height)
        
        # Create pv array
        self.pvarray = OrderedPVArray.init_from_dict(self.pp,param_names=["2","sel","0"])

    def irradiance(self,azi,tilt,gcr,processes_n=1,force_recalc=False):
        '''
        This function calculates the irradience of a panel which is setup on the building.
        param: azi  - azi of the panel
        param: tilt - tilt of the panel
        param: gcr  - Ground coverage ratio
        output: Irradiance (W/m²)
        '''
        fileName = "{:.2f}-{:.2f}-{:.2f}".format(azi,tilt,gcr)
        try:
            if force_recalc:
                raise Exception('Force recalculation')
            # try to open previous rendered file
            while k<=10:
                try:
                    with open('{}{}'.format(self.rendered_files,fileName), 'rb') as f:
                        Irradiance = pickle.load(f)
                except:
                    print("Couldn't read the file")
                    time.sleep(np.random.randint(1,10))
                    if (k==10):
                        raise IOError("Something went wrong reading the irradiance file.")
                    k+=1
                else:
                    break
        except:
            # else generate the irradience and save the file.
            self.pp.update({'surface_azimuth':azi, 'surface_tilt':tilt, 'gcr': gcr})
            # Run the system for an entire year
            # Get the solar position based on the given timestamps
            solar_pos = pvlib.solarposition.get_solarposition(time=self.weather.index,
                                                            latitude=self.meta['latitude'],
                                                            longitude=self.meta['longitude'],
                                                            altitude=self.meta['altitude'], 
                                                            pressure=self.weather['atmospheric_pressure'],
                                                            method='nrel_numpy',
                                                            temperature=self.weather['temp_air'])
            df_inputs = pd.DataFrame(index=self.weather.index)
            df_inputs['solar_zenith']  = solar_pos['zenith']
            df_inputs['solar_azimuth'] = solar_pos['azimuth']
            # Set the tilt and azimuth for the entire year
            df_inputs['surface_tilt']    = tilt
            df_inputs['surface_azimuth'] = azi
            # Set tye solar intensity
            df_inputs['DNI'] = self.weather['dni']
            df_inputs['DHI'] = self.weather['dhi']    
            # 
            #
            with warnings.catch_warnings():
                logger.error("Started Generation of Irradiance")
                warnings.simplefilter("ignore")
                df_new_report = run_parallel_engine(NewReportBuilder14, self.pp, 
                                                df_inputs.index, df_inputs.DNI, df_inputs.DHI,
                                                df_inputs.solar_zenith, df_inputs.solar_azimuth,
                                                df_inputs.surface_tilt, df_inputs.surface_azimuth,
                                                self.pp['albedo'], n_processes=processes_n)
                logger.error("Finished Generation of Irradiance")
            # Make into DataFrame
            report1 = pd.DataFrame(index=self.weather.index,data=df_new_report)
            # Return the data
            Irradiance = report1['total_inc_front'].fillna(0).values
            # Remove any values below zero (some how it will return -10^-5 for some reason).
            Irradiance[Irradiance < 0] = 0
            # Pickle the result to reduce CPU 
            with open('{}{}'.format(self.rendered_files,fileName), 'wb') as f:
                pickle.dump(Irradiance, f)
            # Return the irradiance in W/m2
        return Irradiance
    
    def irradiance_non(self,azi,tilt,gcr,force_recalc=False):
        '''
        This function calculates the irradience of a panel which is setup on the building.
        param: azi  - azi of the panel
        param: tilt - tilt of the panel
        param: gcr  - Ground coverage ratio
        output: Irradiance (W/m²)
        '''
        fileName = "{:.2f}-{:.2f}-{:.2f}".format(azi,tilt,gcr)
        try:
            if force_recalc:
                raise Exception('Force recalculation')
            # try to open previous rendered file
            with open('{}{}'.format(self.rendered_files,fileName), 'rb') as f:
                Irradiance = pickle.load(f)
        except:
            # else generate the irradience and save the file.
            self.pp.update({'surface_azimuth':azi, 'surface_tilt':tilt, 'gcr': gcr})
            # Run the system for an entire year
            # Get the solar position based on the given timestamps
            solar_pos = pvlib.solarposition.get_solarposition(time=self.weather.index,
                                                            latitude=self.meta['latitude'],
                                                            longitude=self.meta['longitude'],
                                                            altitude=self.meta['altitude'], 
                                                            pressure=self.weather['atmospheric_pressure'],
                                                            method='nrel_numpy',
                                                            temperature=self.weather['temp_air'])
            df_inputs = pd.DataFrame(index=self.weather.index)
            df_inputs['solar_zenith']  = solar_pos['zenith']
            df_inputs['solar_azimuth'] = solar_pos['azimuth']
            # Set the tilt and azimuth for the entire year
            df_inputs['surface_tilt']    = tilt
            df_inputs['surface_azimuth'] = azi
            # Set tye solar intensity
            df_inputs['DNI'] = self.weather['dni']
            df_inputs['DHI'] = self.weather['dhi']    
            # 
            #
            self.engine = PVEngine(self.pvarray)
            self.engine.fit(df_inputs.index, df_inputs.DNI, df_inputs.DHI,
                       df_inputs.solar_zenith, df_inputs.solar_azimuth,
                       df_inputs.surface_tilt, df_inputs.surface_azimuth,
                       self.pp['albedo'])
            # Create a function that will build a report
            def fn_report(pvarray): return {'total_inc_front': pvarray.ts_pvrows[1].front.get_param_weighted('qinc').tolist()}

            # Run full mode simulation
            df_new_report = self.engine.run_full_mode(fn_build_report=fn_report)

            # Print results (report is defined by report function passed by user)
            report1 = pd.DataFrame(df_new_report, index=self.weather.index)
            # Return the data
            Irradiance = report1['total_inc_front'].fillna(0).values
            # Remove any values below zero (some how it will return -10^-5 for some reason).
            Irradiance[Irradiance < 0] = 0
            # Pickle the result to reduce CPU 
            with open('{}{}'.format(self.rendered_files,fileName), 'wb') as f:
                pickle.dump(Irradiance, f)
            # Return the irradiance in W/m2
        return Irradiance
    
    def set_north_axis(self,North_Axis):
        # Set the axis azimuth to be equal to the North_Axis
        self.pp.update({'axis_azimuth':North_Axis})
    
    def set_panel_height(self,building_height):
        # Set the panels to float above the building at a height of 1 m
        self.pp.update({'pvrow_height':building_height+1})

class NewReportBuilder12(object):
    """A class is required to build reports when running calculations with
    multiprocessing because of python constraints
    
    This one works with """

    @staticmethod
    def build(report, pvarray):
        # Initialize the report as a dictionary
        if report is None:
            list_keys = ['total_inc_front']
            report = {key: [] for key in list_keys}
        # Add elements to the report
        if pvarray is not None:
            pvrow = pvarray.pvrows[1]  
            report['total_inc_front'].append(
                pvrow.front.get_param_weighted('qinc'))
        else:
            # No calculation was performed, because sun was down
            report['total_inc_front'].append(np.nan)

        return report

    @staticmethod
    def merge(reports):
        """Works for dictionary reports"""
        report = reports[0]
        # Merge other reports
        keys_report = list(reports[0].keys())
        for other_report in reports[1:]:
            for key in keys_report:
                report[key] += other_report[key]
        return report
    
class NewReportBuilder14(object):
    """A class is required to build reports when running calculations with
    multiprocessing because of python constraints
    
    This one works with pvfactors 1.4
    
    """

    @staticmethod
    def build(pvarray):
        # Return front side qinc of second PV row
        return {'total_inc_front': pvarray.ts_pvrows[1].front.get_param_weighted('qinc').tolist()}

    @staticmethod
    def merge(reports):
        """Works for dictionary reports"""
        report = reports[0]
        # Merge other reports
        keys_report = list(reports[0].keys())
        for other_report in reports[1:]:
            for key in keys_report:
                report[key] += other_report[key]
        return report