"""
This code is written to call the python file of of the
paper jupyter notebook. The jupyter notebook runs a
version of the paper. We need to break up the number
of runs to improve.
"""

# Experimental numbers
'''
# 71 - Fill in the missed runs
# 70 - Last run was a succesful one! Got all the data, now I want to submit 200,400,800
# 69 - A new rerun of the previous experiment - now a bit longer so it doesn't cut off some of the runs. And! renamed the export folder.
# 68 - A re-run 50,100 for the calibration
# 67 - 4x 800 - two for cost and two for net.
# 66 - Changed the logging
# 65 - went well
# 65 - 64 went great! did a new setting with the random seed.
# 64 - Each E+ and EH will now output to a seperate folder id-ed with a iteration number.
# 63 - retry and see if the errors are gone.
# 62 - now we have 4 runs at the same time. Let's see if it works.
# 60 was too ambitious, scaling it down to get it to work. Changed the loading,, this unload of the StdEnv2020 is a bit weird.
'''

import papermill as pm
import sys
import time
import logging
import pandas as pd
import os


opt_methods     = ["rbf-cost", "rbf-net"]  # other available "ga-20","ga-50","ga-100"
evaluation_nums = [200,400,800] # number of evaluations during the optmization
iterations      = 10 # number of redo's such that you can see if it works
building_id     = [2] # 2 - Small building
price           = [106] # price of the panels
experiment_nr   = 71  # Random experiment numbers


k=0
option_dict = pd.DataFrame()
for num_eval in evaluation_nums:
    for opt_method in opt_methods:
        for i in range(iterations):
            for bid in building_id:
                for pr in price:
                    option_dict_row = {'k':k,'opt_method': [opt_method], 'number_of_opt_evaluations': [num_eval], "run_i" : [i], "building_id" : [bid], "price" : [pr]}
                    new_row = pd.DataFrame(data=option_dict_row)
                    option_dict=pd.concat([option_dict, new_row])
                    k+=1
print(option_dict)


lrun = logging.getLogger('Runs_logger')
hdlrun = logging.FileHandler(f'{experiment_nr}_runs_logger.txt')
formatterrun = logging.Formatter('%(asctime)s %(message)s')
hdlrun.setFormatter(formatterrun)
lrun.addHandler(hdlrun)
lrun.setLevel(logging.ERROR)




def run_papermill(option_dict, row_num, experiment_nr=experiment_nr):
    print("We are running row_num {}".format(row_num))
    print(option_dict.iloc[row_num])
    opt=str(option_dict.iloc[row_num]["opt_method"])
    eva=int(option_dict.iloc[row_num]["number_of_opt_evaluations"])
    rui=int(option_dict.iloc[row_num]["run_i"])
    bid=int(option_dict.iloc[row_num]["building_id"])
    pr=int(option_dict.iloc[row_num]["price"])
    pm.execute_notebook(
           'cluster_job_pvpaper.ipynb',
           f'{experiment_nr}_{rui}_fast_cluster_test_{opt}_{eva}_{bid}_{pr}.ipynb',
           parameters=dict(opt_method=opt,
                          number_of_opt_evaluations=eva,
                          run_i=rui,iteration=experiment_nr,
                          building_id=bid, price=pr),
    )

print( "This is the name of the script: ", sys.argv[0])
print( "Number of arguments: ", len(sys.argv))
print( "The arguments are: " , str(sys.argv))
job_num=int(sys.argv[1])
# It's important to slow down
# This will stagger the start of the script.
sleeptime=float(job_num)
time.sleep(sleeptime*5)

# Measure the time of the simulation
t1=time.time()

run_papermill(option_dict,job_num)


opt=str(option_dict.iloc[job_num]["opt_method"])
eva=int(option_dict.iloc[job_num]["number_of_opt_evaluations"])
rui=int(option_dict.iloc[job_num]["run_i"])
bid=int(option_dict.iloc[job_num]["building_id"])

endtime=time.time()-t1
mintime=endtime/60
lrun.error("Run nr {} of opt-method {} building_id {} with {} evalautions took: (min)".format(rui,opt,bid,eva))
lrun.error(mintime)
